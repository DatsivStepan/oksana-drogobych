<?php
// HTTP
define('HTTP_SERVER', 'http://oksana-drogobych.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://oksana-drogobych.dev/');

// DIR
define('DIR_APPLICATION', 'C:/wamp/www/oksana-drogobych/catalog/');
define('DIR_SYSTEM', 'C:/wamp/www/oksana-drogobych/system/');
define('DIR_LANGUAGE', 'C:/wamp/www/oksana-drogobych/catalog/language/');
define('DIR_TEMPLATE', 'C:/wamp/www/oksana-drogobych/catalog/view/theme/');
define('DIR_CONFIG', 'C:/wamp/www/oksana-drogobych/system/config/');
define('DIR_IMAGE', 'C:/wamp/www/oksana-drogobych/image/');
define('DIR_CACHE', 'C:/wamp/www/oksana-drogobych/system/cache/');
define('DIR_DOWNLOAD', 'C:/wamp/www/oksana-drogobych/system/download/');
define('DIR_UPLOAD', 'C:/wamp/www/oksana-drogobych/system/upload/');
define('DIR_MODIFICATION', 'C:/wamp/www/oksana-drogobych/system/modification/');
define('DIR_LOGS', 'C:/wamp/www/oksana-drogobych/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'oksana_drogobych');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
